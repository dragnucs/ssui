import Vue from 'vue'

export default api => ({
  state: {
    users: {},
    currentUser: {
      username: '',
      email: '',
      photo: '',
      uid: '',
      firstname: '',
      lastname: ''
    }
  },

  getters: {
    userInfo: state => uid => state.users[uid],
    currentUser: state => state.currentUser
  },

  mutations: {
    addUser (state, { user, uid }) {
      user.usr_uid = uid
      Vue.set(state.users, uid, user)
    },

    setCurrentUser (state, { user }) {
      state.currentUser = user
      window.localStorage.setItem('currentUser', JSON.stringify(user))
    }
  },

  actions: {
    async getUserInfo ({ commit }, { uid }) {
      commit('addUser', await api.getUserInfo(uid))
    },

    setCurrentUser ({ commit }, { user }) {
      commit('setCurrentUser', { user })
    }
  }
})
