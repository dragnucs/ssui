import Vue from 'vue'

export default () => ({
  mutations: {
    addGridRow (state, { variable, columns }) {
      let _columns = columns.map(column => ({ [column.name]: '' }))

      if (!state.variables[variable]) {
        Vue.set(state.variables, variable, {})
      }

      Vue.set(
        state.variables[variable],
        Object.keys(state.variables[variable]).length + 1,
        Object.assign(...Object.entries(_columns).map(([k, v]) => v))
      )
    },

    removeGridRow (state, { variable, rowId }) {
      Vue.delete(state.variables[variable], rowId)
    }
  },

  actions: {
    addGridRow ({ commit }, payload) {
      commit('addGridRow', payload)
    },

    removeGridRow ({ commit }, payload) {
      commit('removeGridRow', payload)
    }
  }
})
