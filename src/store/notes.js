export default (api) => ({
  state: {
    caseNotes: []
  },

  getters: {
    caseNotes: state => state.caseNotes
  },

  mutations: {
    setCaseNotes (state, { caseNotes }) {
      state.caseNotes = caseNotes
    }
  },

  actions: {
    async getCaseNotes ({ commit }, { app }) {
      commit('setCaseNotes', await api.getCaseNotes(app))
    },

    clearCaseNotes ({ commit }) {
      commit('setCaseNotes', { caseNotes: [] })
    },

    async saveCaseNote ({ commit }, { app, note }) {
      api.saveCaseNote(app, note)
    }
  }
})
