export default (api) => ({
  state: {
    inputFiles: [],
    outputFiles: []
  },

  getters: {
    inputFiles: state => state.inputFiles,
    outputFiles: state => state.outputFiles
  },

  mutations: {
    setInputFiles (state, { files }) {
      state.inputFiles = files
    },

    setOutputFiles (state, { files }) {
      state.outputFiles = files
    }
  },

  actions: {
    async uploadFile ({ commit }, { app, task, inputDoc, file }) {
      api.uploadFile(app, task, inputDoc, file)
    },

    async getInputFiles ({ commit }, { app }) {
      commit('setInputFiles', await api.getInputFiles(app))
    },

    clearInputFiles ({ commit }) {
      commit('setInputFiles', { files: [] })
    },

    async getOutputFiles ({ commit }, { app }) {
      commit('setOutputFiles', await api.getOutputFiles(app))
    },

    clearOutputFiles ({ commit }) {
      commit('setOutputFiles', { files: [] })
    }
  }
})
