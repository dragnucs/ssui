import axios from 'axios'

export default (api) => ({
  state: {
    token: ''
  },

  getters: {
    token: state => state.token
  },

  mutations: {
    setToken (state, { token }) {
      state.token = token
      window.localStorage.setItem('token', token)
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    }
  },

  actions: {
    setToken ({ commit }, { token }) {
      commit('setToken', { token })
    },

    async login ({ commit, getters }, { username, password }) {
      commit('setToken', await api.login({ username, password }))
      commit('setCurrentUser', { user: await api.getUser(username) })
    }
  }
})
