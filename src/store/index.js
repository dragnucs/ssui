import Vue from 'vue'
import Vuex from 'vuex'

import Auth from '@/store/auth'
import Cases from '@/store/cases'
import Dynaform from '@/store/dynaform'
import Files from '@/store/files'
import Grid from '@/store/grid'
import Inbox from '@/store/inbox'
import Notes from '@/store/notes'
import Query from '@/store/query'
import Search from '@/store/search'
import Steps from '@/store/steps'
import User from '@/store/user'
import Variables from '@/store/variables'
import ProcessMaker from '@/api/processmaker'

Vue.use(Vuex)

const processmaker = new ProcessMaker({
  serverUri: 'http://172.29.161.56:8091/',
  workspace: 'workflow',
  credentials: {
    grant_type: 'password',
    scope: '*',
    client_id: 'HRONEGZCUGVNLBUCGRXUBOTLUQZIOEKB',
    client_secret: '2005518655c2256439b9459040382570'
  }
})

const auth = new Auth(processmaker)
const cases = new Cases(processmaker)
const dynaform = new Dynaform(processmaker)
const files = new Files(processmaker)
const grid = new Grid(processmaker)
const inbox = new Inbox(processmaker)
const notes = new Notes(processmaker)
const query = new Query(processmaker)
const search = new Search(processmaker)
const steps = new Steps(processmaker)
const user = new User(processmaker)
const variables = new Variables(processmaker)

export default new Vuex.Store({
  state: {
    ...auth.state,
    ...cases.state,
    ...dynaform.state,
    ...files.state,
    ...inbox.state,
    ...notes.state,
    ...query.state,
    ...search.state,
    ...steps.state,
    ...user.state,
    ...variables.state
  },

  getters: {
    ...auth.getters,
    ...cases.getters,
    ...dynaform.getters,
    ...files.getters,
    ...inbox.getters,
    ...notes.getters,
    ...query.getters,
    ...search.getters,
    ...steps.getters,
    ...user.getters,
    ...variables.getters
  },

  mutations: {
    ...auth.mutations,
    ...cases.mutations,
    ...dynaform.mutations,
    ...files.mutations,
    ...grid.mutations,
    ...inbox.mutations,
    ...notes.mutations,
    ...search.mutations,
    ...steps.mutations,
    ...user.mutations,
    ...variables.mutations
  },

  actions: {
    ...auth.actions,
    ...cases.actions,
    ...dynaform.actions,
    ...files.actions,
    ...grid.actions,
    ...inbox.actions,
    ...notes.actions,
    ...query.actions,
    ...search.actions,
    ...steps.actions,
    ...user.actions,
    ...variables.actions
  }
})
