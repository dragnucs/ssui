export default (api) => ({
  state: {
    tasks: [],
    processes: []
  },

  getters: {
    tasks: state => state.tasks,
    processes: state => state.processes
  },

  mutations: {
    setTasks (state, { tasks }) {
      state.tasks = tasks.filter(task => task.tas_type === 'NORMAL')
    },

    setProcesses (state, { processes }) {
      state.processes = processes
    }
  },

  actions: {
    clearTasks ({ commit }) {
      commit('setTasks', { tasks: [] })
    },

    async getTasks ({ commit }, { app }) {
      commit('setTasks', await api.getTasks(app))
    },

    async getProcesses ({ commit, dispatch }) {
      commit('setProcesses', await api.getProcesses())
      dispatch('getTodo')
    }
  }
})
