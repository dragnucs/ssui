export default (api) => ({
  state: {
    dynaform: { items: [{ items: {} }] }
  },

  getters: {
    dynaform: state => state.dynaform
  },

  mutations: {
    setDynaform (state, { dynaform, uid }) {
      state.dynaform = { ...dynaform, uid }
    },

    clearDynaform (state) {
      state.dynaform = { items: [{ items: {} }] }
    }
  },

  actions: {
    clearDynaform ({ commit }) {
      commit('clearDynaform')
    },

    async getDynaform ({ commit, dispatch }, { process, task, app, del, step }) {
      const form = await api.getDynaform(process, task, app, del, step)

      commit('setVariables', { variables: form.variables })
      commit('setDynaform', form.dynaform)
      dispatch('getTasks', { app })
    },

    async submitDynaform ({ commit }, { app }) {
      api.submitDynaform(app)
    }
  }
})
