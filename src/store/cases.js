export default (api) => ({
  state: {
    inbox: [],
    summary: {
      case: {},
      task: {}
    }
  },

  getters: {
    inbox: state => process => {
      return process !== undefined
        ? state.inbox.filter(app => app.pro_uid === process)
        : state.inbox
    },

    newCount: state => process => {
      return state.inbox.filter(app => process === app.pro_uid).length
    },

    overdueCount: state => process => {
      const today = new Date()
      return state.inbox.filter(app => process === app.pro_uid && new Date(app.dueDate.replace(/-/g, ' ')) > today).length
    },

    summary: state => state.summary
  },

  mutations: {
    setInbox (state, { inbox }) {
      state.inbox = inbox
    },

    setSummary (state, { summary }) {
      state.summary = summary
    }
  },

  actions: {
    async createCase ({ commit }, { process }) {
      api.createCase(process)
    },

    async getTodo ({ commit, dispatch }) {
      dispatch('clearInbox')
      commit('setInbox', await api.getTodo())
    },

    clearInbox ({ commit }) {
      commit('setInbox', { inbox: [] })
    },

    async getHistory ({ commit, dispatch }) {
      dispatch('clearInbox')
      commit('setInbox', await api.getHistory())
    },

    async getSummary ({ commit }, { item }) {
      commit('setSummary', await api.getSummary(item))
    }
  }
})
