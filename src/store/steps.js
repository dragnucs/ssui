export default (api) => ({
  state: {
    steps: []
  },

  getters: {
    steps: state => state.steps
  },

  mutations: {
    setSteps (state, { steps }) {
      state.steps = steps
    }
  },

  actions: {
    async getSteps ({ commit }, { app, task }) {
      commit('setSteps', await api.getSteps(app, task))
    }
  }
})
