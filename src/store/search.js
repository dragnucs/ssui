export default (api) => ({
  state: {
    searchResults: []
  },

  getters: {
    searchResults: state => state.searchResults
  },

  mutations: {
    setSearchResults (state, { searchResults }) {
      state.searchResults = searchResults
    }
  },

  actions: {
    async advancedSearch ({ commit }, { keyword }) {
      commit('setSearchResults', await api.search(keyword))
    }
  }
})
