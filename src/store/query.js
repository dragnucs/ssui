export default (api) => ({
  actions: {
    async executeQuery ({ getters }, { process, field, variable }) {
      return api.executeQuery(process, field, variable, getters.dynaform.uid)
    },

    async executeQuerySuggest ({ getters }, { process, field, variable, filter }) {
      return api.executeQuerySuggest(process, field, variable, filter, getters.dynaform.uid)
    }
  }
})
