import Vue from 'vue'

export default (api) => ({
  state: {
    variables: {}
  },

  getters: {
    variables: state => state.variables
  },

  mutations: {
    setVariables (state, { variables }) {
      state.variables = variables
    },

    updateVariable (state, { variable, value }) {
      Vue.set(state.variables, variable, value)
    }
  },

  actions: {
    async saveVariables ({ getters }, { app }) {
      api.saveVariables(app, getters.variables)
    },

    updateVariable ({ commit }, payload) {
      commit('updateVariable', payload)
    }
  }
})
