import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Buefy from 'buefy'

import '@mdi/font/scss/materialdesignicons.scss'
import 'typeface-fira-sans'
import './registerServiceWorker'

Vue.config.productionTip = false

Vue.use(Buefy)

router.beforeEach((to, from, next) => {
  if (to.name === 'login') {
    next()
  }

  const token = window.localStorage.getItem('token')
  const user = JSON.parse(window.localStorage.getItem('currentUser'))

  if ((token !== 'null' || user === null) && to.name !== 'login') {
    next({ name: 'login' })
  }

  store.dispatch('setToken', { token })
  store.dispatch('setCurrentUser', { user })

  next()
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
