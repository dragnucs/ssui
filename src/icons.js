import lcs from '@/assets/lcs.png'
import tms from '@/assets/tms.png'
import opr from '@/assets/opr.png'
import testflow from '@/assets/testflow.png'
import cep from '@/assets/cep.png'
import sync from '@/assets/sync.png'
import ers from '@/assets/ers.png'

export default {
  get (uid) {
    return this.icons[uid]
  },

  icons: {
    '1897636705bfec219be2a87060503704': tms,
    '3901176515c3f049cdbb646080543095': lcs,
    '2580787335c21f34dc10b89017700151': opr,
    '3552438785c508e5a651f96066503723': testflow,
    '3791625205cab1fbc4310a5014445661': cep,
    '1085685215ccab8f06d4805038355732': sync,
    '9282711365cf4f3676c0a30065369820': ers
  }
}
