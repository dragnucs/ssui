export default {
  methods: {
    caseRoute (item) {
      return {
        name: 'inbox-form',
        params: {
          process: item.pro_uid,
          task: item.task_uid,
          app: item.app_uid,
          del: item.del_index
        }
      }
    }
  }
}
