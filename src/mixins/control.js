export default {
  props: {
    label: {
      type: String,
      required: true
    },

    variable: {
      type: String,
      required: false,
      default: null
    },

    value: {
      type: String,
      required: false,
      default: ''
    },

    placeholder: {
      type: String,
      required: false,
      default: ''
    },

    mode: {
      type: String,
      required: true
    },

    hint: {
      type: String,
      required: false,
      default: ''
    },

    parentMode: {
      type: String,
      required: false,
      default: 'edit'
    },

    /**
     * Naming this prop _mandatory_ instead of _required_ to avoid conflicts.
     */
    mandatory: {
      type: Boolean,
      required: true
    }
  },

  computed: {
    _value: {
      get () {
        return this.value
      },

      set (value) {
        this.$emit('update', { variable: this.variable, value })
      }
    },

    isDisabled () {
      return this.mode === 'parent'
        ? this.parentMode === 'disabled'
        : this.mode === 'disabled'
    },

    isReadonly () {
      return this.mode === 'parent'
        ? this.parentMode === 'view'
        : this.mode === 'view'
    }
  }
}
