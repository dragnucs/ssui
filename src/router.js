import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/Home.vue')
    }, {
      path: '/login',
      name: 'login',
      props: true,
      component: () => import('./views/Login.vue')
    }, {
      path: '/logout',
      name: 'logout',
      props: true,
      component: () => import('./views/Logout.vue')
    }, {
      path: '/create/:process',
      name: 'create',
      props: true,
      component: () => import('./views/Create.vue')
    }, {
      path: '/inbox/',
      props: true,
      component: () => import('./views/Inbox.vue'),
      children: [{
        path: 'history/',
        name: 'history-list',
        props: {
          default: true,
          dynaform: { process: 'history' },
          list: { process: 'history' }
        },
        components: {
          list: () => import('./components/Items/List.vue'),
          dynaform: () => import('./components/EmptySelection.vue')
        },
        meta: { list: 'history' }
      }, {
        path: 'history/:process/:task/:app/:del',
        name: 'history-form',
        props: { default: true, dynaform: true, list: true },
        components: {
          list: () => import('./components/Items/List.vue'),
          dynaform: () => import('./views/Dynaform.vue')
        },
        meta: { list: 'history' }
      }, {
        path: ':process/',
        name: 'inbox-list',
        props: { default: true, dynaform: true, list: true },
        components: {
          list: () => import('./components/Items/List.vue'),
          dynaform: () => import('./components/EmptySelection.vue')
        },
        meta: { list: 'inbox' }
      }, {
        path: ':process/:task/:app/:del',
        name: 'inbox-form',
        props: { default: true, dynaform: true, list: true },
        components: {
          list: () => import('./components/Items/List.vue'),
          dynaform: () => import('./views/Dynaform.vue')
        },
        meta: { list: 'inbox' }
      }]
    }, {
      path: '/search/:keyword',
      name: 'search',
      props: true,
      component: () => import('./views/Search.vue')
    }
  ]
})
